#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

// GLOBAL VARIABLE
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 800;

// DEKLARASI VERTEX SHADER 
const char* vertexShaderSource = "#version 400\n"\
"layout(location=0) in vec4 in_Position;\n"\
"layout(location=1) in vec4 in_Color;\n"\
"out vec4 ex_Color;\n"\

"void main(void)\n"\
"{\n"\
"  gl_Position = in_Position;\n"\
"  ex_Color = in_Color;\n"\
"}\n";

//DEKLARASI FRAGMENT SHADER
const char* fragmentShaderSource =  "#version 400\n"\
  "in vec4 ex_Color;\n"\
  "out vec4 out_Color;\n"\

  "void main(void)\n"\
  "{\n"\
  "  out_Color = ex_Color;\n"\
  "}\n";

// STRUCT VERTEX
typedef struct
{
    float XYZW[4];
    float RGBA[4];
} Vertex;

int main()
{

    // SETTING UP GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // WINDOW CREATION
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "4210181018_LABWORK12_EBO", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // LOAD GLFW POINTER
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // VERTEX SHADER
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    // CHECK COMPILE ERROR
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // FRAGMENT SHADER
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    // CHECK COMPILE ERROR
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // LINK SHADER
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    // CHECK ERROR LINK SHADER
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);


    // SET UP VERTEX & BUFFER DATA

    // DEKLARASI VERTICES
    Vertex vertices[] = { 
        { { 0.0f, 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f } },
        // Top
        { { -0.2f, 0.8f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
        { { 0.2f, 0.8f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } },
        { { 0.0f, 0.8f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f, 1.0f } },
        { { 0.0f, 1.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
        // Bottom
        { { -0.2f, -0.8f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } },
        { { 0.2f, -0.8f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
        { { 0.0f, -0.8f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f, 1.0f } },
        { { 0.0f, -1.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
        // Left
        { { -0.8f, -0.2f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
        { { -0.8f, 0.2f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } },
        { { -0.8f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f, 1.0f } },
        { { -1.0f, 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
        // Right
        { { 0.8f, -0.2f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } },
        { { 0.8f, 0.2f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
        { { 0.8f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f, 1.0f } },
        { { 1.0f, 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } }
    };
    

    // DEKLARASI INDEX
    GLubyte Indices[] = {
        // Top
        0, 1, 3,
        0, 3, 2,
        3, 1, 4,
        3, 4, 2,
        // Bottom
        0, 5, 7,
        0, 7, 6,
        7, 5, 8,
        7, 8, 6,
        // Left
        0, 9, 11,
        0, 11, 10,
        11, 9, 12,
        11, 12, 10,
        // Right
        0, 13, 15,
        0, 15, 14,
        15, 13, 16,
        15, 16, 14
    };

    // DEKLARASI VAO, VBO, dan EBO
    unsigned int VBO, VAO, indexBuffer;

    // VAO CONFIG
    glGenVertexArrays(1, &VAO); // GENERATE VERTEX ARRAY
    glBindVertexArray(VAO); // BINDING VERTEX ARRAY KE VARIABEL VAO

    //VBO CONFIG
    glGenBuffers(1, &VBO); // GENERATE VERTEX BUFFER UNTUK MEMESAN MEMORI DI GPU
    glBindBuffer(GL_ARRAY_BUFFER, VBO); // BINDING BUFFER ARRAY KE VBO
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // MENGISI DATA ARRAY BUFFER SESUAI DENGAN VARIABEL ARRAY VERTICES

    // EBO CONFIG
    glGenBuffers(1, &indexBuffer); // GENERATE ELEMENT BUFFER UNTUK MEMESAN MEMORI DI GPU
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer); // BINDING ELEMENT ARRAY BUFFER KE EBO / INDEX BUFFER
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW); // MENGISI DATA ELEMENT ARRAY BUFFER SESUAI DENGAN VARIABEL INDICES / INDEX

    // ENABLE ATTRIBUT VERTEX ARRAY UNTUK OBJEK
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), 0); // DEKLARASI POINTER UNTUK ATTRIBUTE ARRAY OBJEK
    glEnableVertexAttribArray(0); // ENABLE / DISABLE ARRAY ATRIBUTE VERTEX

    // ENABLE ATTRIBUT VERTEX ARRAY UNTUK WARNA
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)sizeof(vertices[0].XYZW)); // DEKLARASI POINTER UNTUK ATTRIBUTE ARRAY WARNA
    glEnableVertexAttribArray(1); // ENABLE / DISABLE ARRAY ATRIBUTE VERTEX
   

   // RENDERING
    while (!glfwWindowShouldClose(window))
    {
        
        processInput(window);

        // CLEAR COLOR
        glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // DRAW 
        glUseProgram(shaderProgram);
        glBindVertexArray(VAO); 

        //glDrawArrays(GL_TRIANGLES, 0, 6); 

        // MENGGUNAKAN glDrawElements KARENA URUTAN PENGGAMBARAN MENGGUNAKAN ELEMEN DENGAN PARAMETER UKURAN SEBANYAK VERTEX YANG AKAN DIGUNAKAN
        glDrawElements(GL_TRIANGLES, 48, GL_UNSIGNED_BYTE, (GLvoid*)0);
        glBindVertexArray(0);
        

        // SWAP BUFFER IO
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // DELETE SEMUA BUFFER
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &indexBuffer);
    glDeleteProgram(shaderProgram);

    // TERMINATE GLFW
    glfwTerminate();
    return 0;
}

// KEY PRESSED
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// WINDOW RESIZE
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}