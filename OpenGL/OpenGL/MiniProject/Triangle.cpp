#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <cstdlib>
#include <cmath>
#include <iostream>
#include "Triangle.h"

Triangle::Triangle() {
	width = 10;
	height = 10;

	x = 0;
	y = 0;

	red = 1;
	green = 0.647;
	blue = 0;

	speed = 1;
}

void Triangle::Draw() {

	glPushMatrix();
	glTranslatef(tx, ty, 0);
	glBegin(GL_POLYGON);

	glColor3f(red, green, blue);

	glVertex3f(x, y, 0); // bawah
	glVertex3f(x - width / 2, y + height, 0); // kiri
	glVertex3f(x + width / 2, y + height, 0); //kanan

	glEnd();
	glPopMatrix();
}

void Triangle::Translate(float tx, float ty) {

	this->ty += ty * speed;
	if (this->ty < -120 - y) {
		this->ty = 100 - y;
		speed = rand() % 3 + 1;
	}
	glutPostRedisplay();
}

void Triangle::setWidth(float w) {
	width = w;
}

void Triangle::setHeight(float h) {
	height = h;
}

float Triangle::getWidth() {
	return width;
}

float Triangle::getHeight() {
	return height;
}

void Triangle::setSpeed(float spd) {
	speed = spd;
}

float Triangle::getSpeed() {
	return speed;
}

