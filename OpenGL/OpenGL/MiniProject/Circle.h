#pragma once
#include "Object.h"

class Circle : public Object {
private:
	float r;

public:
	Circle();

	void setR(float r);
	float getR();

	void Draw();

	void Translate(float tx, float ty);
 };