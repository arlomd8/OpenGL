#include"Object.h"
#include <iostream>

Object::Object() {
	x = 0;  
	y = 0;

	tx = 0;
	ty = 0;

	red = 0;
	green = 0;
	blue = 0;
}

//TRANSLASI
void Object::Translate(float tx, float ty) {
	this->tx += tx;
	this->ty += ty;
}

float Object::getTY() {
	return ty;
}

float Object::getTX() {
	return tx;
}

void Object::setTY(float ty) {
	this->ty = ty;
}

void Object::setTX(float tx) {
	this->tx = tx;
}

//KOORDINAT
void  Object::setX(float ix) {
	x = ix;
}

void  Object::setY(float iy) {
	y = iy;
}

float Object::getX() {
	return x;
}

float Object::getY() {
	return y;
}


//COLOR

void Object::SetColor(float r, float g, float b) {
	red = r;
	green = g;
	blue = b;
}


