#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <cstdlib>
#include <cmath>
#include <iostream>
#include "Circle.h"

#define PI 3.14159265

Circle::Circle() {
	r = 10;

	x = 0;
	y = 0;

	red = 0;
	green = 0;
	blue = 1;
}

void Circle::setR(float radius) {
	r = radius;
}

float Circle::getR() {
	return r;
}

void Circle::Draw() {

	float angle = 0;

	glPushMatrix();
	glTranslatef(tx, ty, 0);

	glBegin(GL_POLYGON);
	for (int i = 0; i < 32; ++i) {
		glColor3f(red, green, blue);
		glVertex3f(x + r * cos(angle),
				   y + r * sin(angle),
				   0);

		angle += 2 * PI / 30;
	}

	glEnd(); 
	glPopMatrix();
}

void Circle::Translate(float tx, float ty) {
	
	float batas_kiri = this->tx + tx + x - r;
	float batas_kanan = this->tx + tx + x + r;

	if ((batas_kanan >= 100 && tx > 0) ||
		(batas_kiri <= -100 && tx < 0)) {
		tx = 0;
	}
	this->tx += tx;
}