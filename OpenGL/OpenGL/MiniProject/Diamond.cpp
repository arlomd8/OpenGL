#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <cstdlib>
#include <cmath>
#include <iostream>
#include "Diamond.h"

Diamond::Diamond() {
	width = 10;
	height = 10;

	x = 0;
	y = 0;

	red = 0;
	green = 1;
	blue = 0;

	speed = 1;
}

void Diamond::Draw() {

	glPushMatrix();
	glTranslatef(tx, ty, 0);
	glBegin(GL_QUADS);

	glColor3f(red, green, blue);

	glVertex3f(x, y, 0); // bawah
	glVertex3f(x - width / 2, y + height / 2, 0); // kiri
	glVertex3f(x, y + height, 0); //atas
	glVertex3f(x + width / 2, y + height / 2, 0); //kanan

	glEnd();
	glPopMatrix();
}

void Diamond::Translate(float tx, float ty) {

	this->ty += ty * speed;
	if (this->ty < -120 - y) {
		this->ty = 100 - y;
		speed = rand() % 3 + 1;
	}
	glutPostRedisplay();
}

void Diamond::setWidth(float w) {
	width = w;
}

void Diamond::setHeight(float h) {
	height = h;
}

float Diamond::getWidth() {
	return width;
}

float Diamond::getHeight() {
	return height;
}

void Diamond::setSpeed(float spd) {
	speed = spd;
}

float Diamond::getSpeed() {
	return speed;
}


