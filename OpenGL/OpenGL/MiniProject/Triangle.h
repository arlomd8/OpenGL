#pragma once
#include "Object.H"

class Triangle : public Object {
private:
	float width;
	float height;
	float speed;

public:
	Triangle();

	void Draw();

	void setWidth(float w);
	float getWidth();

	void setHeight(float h);
	float getHeight();

	void setSpeed(float spd);
	float getSpeed();

	void Translate(float tx, float ty);

};
