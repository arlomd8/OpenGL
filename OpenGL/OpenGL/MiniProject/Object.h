#pragma once
class Object
{
protected:
	float x;
	float y;

	float tx;
	float ty;
	
	float red;
	float green;
	float blue;

public:
	Object();

	void Draw();

	void setX(float ix);
	void setY(float iY);
	float getX();
	float getY();

	void Translate(float tx, float ty);
	float getTX();
	float getTY();

	void setTX(float tx);
	void setTY(float ty);

	void SetColor(float r, float g, float b);
};


