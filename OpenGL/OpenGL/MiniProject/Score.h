#pragma once

class Score {
private:
	static Score *instance;
	int score;
	Score();
public:
	static Score* getInstance();
	int getScore();
	void setScore(int score);
};
