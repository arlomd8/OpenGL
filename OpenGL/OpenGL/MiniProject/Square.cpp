#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <cstdlib>
#include <cmath>
#include <iostream>
#include "Square.h"

Square::Square() {
	width = 10;
	height = 10;

	x = 0;
	y = 0;

	red = 1;
	green = 0;
	blue = 0;
	
	speed = 1;
}

void Square::Draw() {
	
	glPushMatrix();
	glTranslatef(tx, ty, 0);
	glBegin(GL_QUADS);

	glColor3f(red, green, blue);

	glVertex3f(x, y, 0); 
	glVertex3f(x + width, y, 0);
	glVertex3f(x + width, y + height, 0);
	glVertex3f(x, y+height, 0);

	glEnd();
	glPopMatrix();
}

void Square::Translate(float tx, float ty) {
	this->ty += ty * speed;
	if (this->ty < -120 - y) {
		this->ty = 100 - y;
		speed = rand() % 3 + 1;

	}
	glutPostRedisplay();
}

void Square::setWidth(float w) {
	width = w;
}

void Square::setHeight(float h) {
	height = h;
}

float Square::getWidth() {
	return width;
}

float Square::getHeight() {
	return height;
}


void Square::setSpeed(float spd) {
	speed = spd;
}

float Square::getSpeed() {
	return speed;
}

