#include <iostream>
#include <algorithm>
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <chrono>
#include <thread>
#include <functional>
#include <algorithm>
#include <time.h> 
#include <windows.h>
#include <string.h>
#include <string>

#include "Circle.h"
#include "Square.h"
#include "Diamond.h"
#include "Triangle.h"

using namespace std;

Circle player;
Square enemySquare;
Diamond enemyDm, enemyDm2;
Triangle enemyTr, enemyTr2;

const int font = (int)GLUT_BITMAP_HELVETICA_18;

int life = 3;
static int score = 0;
bool startGame = false;
bool gamePaused = false;
bool collide = true;

static int isAnimate = 0;
static int animationPeriod = 50;

bool collisionSquare(Circle player, Square obstacle) {
	float deltaX = player.getX() + player.getTX() - max(obstacle.getX() + obstacle.getTX(), min(player.getX() + player.getTX(), obstacle.getX() + obstacle.getWidth() + obstacle.getTX()));
	float deltaY = player.getY() + player.getTY() - max(obstacle.getY() + obstacle.getTY(), min(player.getY() + player.getTY(), obstacle.getY() + obstacle.getHeight() + obstacle.getTY()));
	return (deltaX * deltaX + deltaY * deltaY) < (player.getR() * player.getR());
}

bool collisionDiamond(Circle player, Diamond obstacle) {
	float deltaX = player.getX() + player.getTX() - max(obstacle.getX() + obstacle.getTX(), min(player.getX() + player.getTX(), obstacle.getX() + obstacle.getWidth() + obstacle.getTX()));
	float deltaY = player.getY() + player.getTY() - max(obstacle.getY() + obstacle.getTY(), min(player.getY() + player.getTY(), obstacle.getY() + obstacle.getHeight() + obstacle.getTY()));
	return (deltaX * deltaX + deltaY * deltaY) < (player.getR() * player.getR());
}

bool collisionTriangle(Circle player, Triangle obstacle) {
	float deltaX = player.getX() + player.getTX() - max(obstacle.getX() + obstacle.getTX(), min(player.getX() + player.getTX(), obstacle.getX() + obstacle.getWidth() + obstacle.getTX()));
	float deltaY = player.getY() + player.getTY() - max(obstacle.getY() + obstacle.getTY(), min(player.getY() + player.getTY(), obstacle.getY() + obstacle.getHeight() + obstacle.getTY()));
	return (deltaX * deltaX + deltaY * deltaY) < (player.getR() * player.getR());
}

void RenderBitmapString(float x, float y, void* font, const char* string) {
	const char* c;
	glRasterPos2f(x, y);
	for (c = string; *c != '\0'; c++) {
		glutBitmapCharacter(font, *c);
	}
}

void printInteraction(void)
{
	system("CLS");
	cout << "INSTRUCTION" << endl << "\n";
	cout << "AVOID THE OBSTACLE AND DONT COLLIDE" << endl << "\n";
	cout << "PRESS SPACE TO START/PAUSE GAME" << endl
		<< "PRESS A TO MOVE LEFT" << endl
		<< "PRESS D TO MOVE RIGHT" << endl << "\n";

	cout << "SCREENSHAPER BY " << endl
		<< "ARLO MARIO DENDI - 4210181018" << endl
		<< "SULTHAN MAHENDRA M - 4210181019" << endl << "\n";


	cout << "Score: " << score << endl;
	cout << "Lives: " << life << endl;
	if (life == 0) {
		cout << "GAME OVER" << endl;
	}

}

void DrawTextScore(void) {
	char buffer[50];
	sprintf_s(buffer, "SCORE : %d", score);
	glColor3f(0, 0, 0);
	RenderBitmapString(-98, 95, (void*)font, buffer);

	glFlush();
}

void DrawTextLives(void) {
	char buffer[50];
	sprintf_s(buffer, "LIVES : %d", life);
	glColor3f(0, 0, 0);
	RenderBitmapString(-98, 85, (void*)font, buffer);

	glFlush();
}

void DisplayText(void) {
	glColor3f(0, 0, 0);
	RenderBitmapString(-98, 95, (void*)font, "SCORE : ");
	RenderBitmapString(-98, 85, (void*)font, "LIVES : ");

	if (isAnimate && !gamePaused) {
		RenderBitmapString(-55, 0, (void*)font, " ");
	}
	else if (!startGame) {
		RenderBitmapString(-55, 20, (void*)font, "AVOID THE OBSTACLE AND DONT COLLIDE");
		RenderBitmapString(-32, 10, (void*)font, "PRESS D TO MOVE RIGHT");
		RenderBitmapString(-30, 0, (void*)font, "PRESS A TO MOVE LEFT");
		RenderBitmapString(-55, -10, (void*)font, "PRESS SPACE TO START AND PAUSE GAME");
	}
	else if (gamePaused) {
		RenderBitmapString(-19, 0, (void*)font, "GAME PAUSED");
		RenderBitmapString(-34, -10, (void*)font, "PRESS SPACE TO RESUME");
	}
	else if (collide && life > 0) {
		RenderBitmapString(-30, 10, (void*)font, "YOU HIT THE OBSTACLE");
	}
	else if (life == 0) {
		RenderBitmapString(-17, 10, (void*)font, "GAME OVER");
		RenderBitmapString(-18, -10, (void*)font, "PRESS SPACE");
	}

	glFlush();
}

void animate(int value)
{

	if (isAnimate && !gamePaused)
	{
		enemySquare.Translate(0, -1);
		enemyDm.Translate(0, -1);
		enemyDm2.Translate(0, -1);
		enemyTr.Translate(0, -1);
		enemyTr2.Translate(0, -1);

		if (enemySquare.getTY() <= -120 - enemySquare.getY()) {
			score += 1;
			printInteraction();
		}

		if (enemyDm.getTY() <= -120 - enemyDm.getY()) {
			score += 1;
			printInteraction();
		}

		if (enemyDm2.getTY() <= -120 - enemyDm2.getY()) {
			score += 1;
			printInteraction();
		}

		if (enemyTr.getTY() <= -120 - enemyTr.getY()) {
			score += 1;
			printInteraction();
		}

		if (enemyTr2.getTY() <= -120 - enemyTr2.getY()) {
			score += 1;
			printInteraction();
		}

		if (collisionSquare(player, enemySquare) ||
			collisionDiamond(player, enemyDm) ||
			collisionDiamond(player, enemyDm2) ||
			collisionTriangle(player, enemyTr) ||
			collisionTriangle(player, enemyTr2)) {

			isAnimate = 0;
			collide = true;
			life -= 1;
			printInteraction();
			glutPostRedisplay();
		}

		glutTimerFunc(animationPeriod, animate, 1);
	}
}

float randomSpeed() {
	return rand() % 3 + 1;
}



void DrawScene(void) {

	glClear(GL_COLOR_BUFFER_BIT);

	player.setX(0);
	player.setY(-50);
	player.setR(10);
	player.Draw();

	enemySquare.setX(-10);
	enemySquare.setY(102);
	enemySquare.setWidth(20);
	enemySquare.setHeight(20);
	enemySquare.Draw();

	enemyDm.setX(-35);
	enemyDm.setY(132);
	enemyDm.setWidth(20);
	enemyDm.setHeight(20);
	enemyDm.Draw();

	enemyDm2.setX(35);
	enemyDm2.setY(162);
	enemyDm2.setWidth(20);
	enemyDm2.setHeight(20);
	enemyDm2.Draw();

	enemyTr.setX(70);
	enemyTr.setY(192);
	enemyTr.setWidth(20);
	enemyTr.setHeight(20);
	enemyTr.Draw();

	enemyTr2.setX(-70);
	enemyTr2.setY(222);
	enemyTr2.setWidth(20);
	enemyTr2.setHeight(20);
	enemyTr2.Draw();

	DisplayText();
	DrawTextScore();
	DrawTextLives();

	glFlush();
}



void Setup() {
	glClearColor(1.0, 1.0, 1.0, 0.0);
}

void Resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-100.0, 100.0, -100.0, 100.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void PlayerMovement(unsigned char key, int x, int y) {
	switch (key) {
	case 27:
		exit(0);
		break;
	case 'd':
		if (!gamePaused && isAnimate) {
			player.Translate(35, 0);
		}
		glutPostRedisplay();
		break;
	case 'a':
		if (!gamePaused && isAnimate) {
			player.Translate(-35, 0);
		}
		glutPostRedisplay();
		break;
	case 'D':
		player.Translate(35, 0);
		glutPostRedisplay();
		break;
	case 'A':
		player.Translate(-35, 0);
		glutPostRedisplay();
		break;
	case ' ':
		if (gamePaused && isAnimate) {
			gamePaused = 0;
			animate(1);
		}
		else if (!gamePaused && isAnimate) {
			gamePaused = 1;

		}
		else
		{
			if (collide)
			{
				collide = false;
				if (!startGame) startGame = true;

				player.setTX(0);
				enemySquare.setTY(0);
				enemyDm.setTY(0);
				enemyDm2.setTY(0);
				enemyTr.setTY(0);
				enemyTr2.setTY(0);

				enemySquare.setSpeed(1);
				enemyDm.setSpeed(1);
				enemyDm2.setSpeed(1);
				enemyTr.setSpeed(1);
				enemyTr2.setSpeed(1);


				if (life == 0) {
					life = 3;
					score = 0;
					printInteraction();
				}

			}
			isAnimate = 1;
			animate(1);
			
		}
		glutPostRedisplay();
		break;
	default:
		break;
	}
}





int main(int argc, char** argv) {

	printInteraction();
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(700, 700);
	glutInitWindowPosition(200, 200);
	glutCreateWindow("ScreenShaper");
	glutDisplayFunc(DrawScene);

	srand(time(NULL));

	glutReshapeFunc(Resize);
	glutKeyboardFunc(PlayerMovement);

	glewExperimental = GL_TRUE;
	glewInit();

	Setup();
	glutMainLoop();
}
