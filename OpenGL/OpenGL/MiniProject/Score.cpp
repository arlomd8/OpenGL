#include <iostream>
#include "Score.h"
using namespace std;

Score::Score() {
	score = 0;
}

Score* Score::getInstance() {
	if (!instance)
		instance = new Score;
	return instance;
}

int Score::getScore() {
	return this->score;
}

void Score::setScore(int score) {
	this->score = score;
}
Score* Score::instance = 0;