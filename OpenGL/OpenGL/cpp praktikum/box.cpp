/////////////////////////////////          
// box.cpp 
// 4210181018_Teori3.cpp
//
// This program draws a wire box.
//
// Sumanta Guha.
/////////////////////////////////

#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h> 

// Drawing routine.
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT); //Membersihkan buffer
	glColor3f(0.0, 0.0, 0.0); // Memberi warna pada buffer
	glLoadIdentity(); // Memuat matrix identitas

	
	
	// Model Transformasi
	glTranslatef(0.0, 0.0, -15.0); // Mentranslasikan/memindahkan objek ke koordinat yang dituju. 
								   // Fungsi tersebut akan mengalikan matrix saat ini dengan matrix translasi umum
								   // Parameter yang dibutuhkan adalah koordinat tujuan yaitu x,y, dan z
								   // Jika dituliskan dalam matematika berbentuk seperti (x',y',z') = (a, b, c) + (x, y, z)
								   // (a,b,c) adalah vektor translasi, (x',y',z') koordinat baru, (x,y,z) koordinat awal.

	glRotatef(60.0, 0.0, 0.0, 1.0); // Fungsi yang digunakan untuk merotasi objek terhadap tiga sumbu x,y, dan z dengan sudut yang ditentukan.
									// Fungsi tersebut akan mengalikan matrix saat ini dengan matrix rotasi umum
									// Parameter yang digunakan adalah sudut, sumbu x,y, dan z
								    // Jika dituliskan dalam matematika berbentuk seperti (kx = k(x-a) + a, ky = k(y-b) + b, kz = k(z-c) + c) 
									// Dengan k adalah faktor perbesaran objek. (a,b,c) pusat dilatasi, (x,y,z) titik koordinat awal

	glutWireTeapot(5.0); // Perintah yang digunakan menggambar kerangka kubus dengan parameter ukuran kerangka kubus dengan tipe data double

	glFlush(); // Melaksanakan perintah untuk melakukan rendering
}

// Initialization routine.
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); //Membersihkan kanvas dan memberi warna pada kanvas.
}


// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);

	glMatrixMode(GL_MODELVIEW);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

// Main routine.
int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Teori3.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}

