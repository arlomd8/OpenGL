
#include <GL/glew.h>
#include <GL/freeglut.h> 

void drawScene(void)
{

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);//KOTAK KE 1 BERWARNA MERAH
	glBegin(GL_POLYGON); //MENGGUNAKAN FUNGSI PRIMITIF GL POLYGON
	glVertex3f(20.0, 20.0, 0.0); //KOORDINAT TITIK A(20,20)
	glVertex3f(80.0, 20.0, 0.0); //KOORDINAT TITIK B(80,20)
	glVertex3f(80.0, 80.0, 0.0); //KOORDINAT TITIK C(80,80)
	glVertex3f(20.0, 80.0, 0.0); //KOORDINAT TITIK D(20,80)
	glEnd(); //UNTUK MENGAKHIRI PENGGAMBARAN


	glColor3f(0.0, 1.0, 0.0);//KOTAK KE 2 BERWARNA HIJAU
	glBegin(GL_POLYGON); //MENGGUNAKAN FUNGSI PRIMITIF GL POLYGON
	glVertex3f(40.0, 40.0, 0.0); //KOORDINAT TITIK A(40,40)
	glVertex3f(60.0, 40.0, 0.0); //KOORDINAT TITIK B(60,40)
	glVertex3f(60.0, 60.0, 0.0); //KOORDINAT TITIK C(60,60)
	glVertex3f(40.0, 60.0, 0.0); //KOORDINAT TITIK D(40,60)
	glEnd(); //UNTUK MENGAKHIRI PENGGAMBARAN



	glFlush(); //UNTUK MELAKSANAKAN RENDERING
}


void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); //UNTUK MEMBERSIHKAN KANVAS DAN MENGGANTI WARNA PADA KANVAS
}


void resize(int w, int h)
{
	glViewport(0, 0, w, h);//JARAK PANDANG TERHADAP OBJEK

	glMatrixMode(GL_PROJECTION);// MENGGUNAKAN PROYEKSI
	glLoadIdentity();//MEMUAT MATRIX IDENTITAS
	glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);// PROYEKSI ORTHOGONAL

	glMatrixMode(GL_MODELVIEW);// MENGGUNAKAN GL_MODELVIEW
	glLoadIdentity(); //MEMUAT MATRIX IDENTITAS
}


void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}


int main(int argc, char** argv)
{
	glutInit(&argc, argv); //INISIALISASI GLUT

	glutInitContextVersion(4, 3);//INISIALISASI DAN SETTING GLUT
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);//MENGANTISIPASI FUNGSI OPENGL YANG TERDEPRECATED DI VERSI BARU

	//INISIALISASI WINDOW DENGAN UKURAN DAN JUDUL WINDOW
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_PO1");

	glutDisplayFunc(drawScene);//MEMANGGIL FUNGSI DISPLAY
	glutReshapeFunc(resize);// MEMANGGIL FUNGSI RESIZE
	glutKeyboardFunc(keyInput);// MEMANGGIL FUNGSI KEYBOARD HANDLER
	
	//INISIALISASI GLEW
	glewExperimental = GL_TRUE;
	glewInit();

	setup();//PEMANGGILAN FUNGSI SETUP

	glutMainLoop();// PEMANGGILAN GLUT MAIN LOOP
}
