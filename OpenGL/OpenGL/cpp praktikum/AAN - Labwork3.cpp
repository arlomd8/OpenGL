#include <iostream>
#include <stdlib.h>
#include <ctime>

#include <GL/glew.h>
#include <GL/freeglut.h> 
using namespace std;

static unsigned int offsets[9] = { 0, 1, 2, 3, 4, 5, 6, 0, 6 };
static unsigned int object;

// Initialization routine.
void setup(void)
{
	srand((unsigned)time(0));
	object = glGenLists(6);
	float random, range;

	// Square Blue 1
	glNewList(object, GL_COMPILE);
	range = 25;
	random = rand() % 50 + (-50);

	glColor3f(0.0, 0.4, 0.7);
	glBegin(GL_POLYGON);
	glVertex3f(random, random, 0.0);
	glVertex3f(random + range, random, 0.0);
	glVertex3f(random + range, random + range, 0.0);
	glVertex3f(random, random + range, 0.0);
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Square Blue 2
	glNewList(object + 1, GL_COMPILE);
	range = 15;
	random = rand() % 50 + (-50);

	glColor3f(0.0, 0.4, 0.7);
	glBegin(GL_POLYGON);
	glVertex3f(random, random, 0.0);
	glVertex3f(random + range, random, 0.0);
	glVertex3f(random + range, random + range, 0.0);
	glVertex3f(random, random + range, 0.0);
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Square Blue 3
	glNewList(object + 2, GL_COMPILE);
	range = 10;
	random = rand() % 50 + (-50);

	glColor3f(0.0, 0.4, 0.7);
	glBegin(GL_POLYGON);
	glVertex3f(random, random, 0.0);
	glVertex3f(random + range, random, 0.0);
	glVertex3f(random + range, random + range, 0.0);
	glVertex3f(random, random + range, 0.0);
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Diamond Green 1
	glNewList(object + 3, GL_COMPILE);
	range = 10;
	random = rand() % 50 + (-50);

	glColor3f(0.0, 0.8, 0.4);
	glBegin(GL_POLYGON);
	if (random < 0) {
		glVertex3f(random, random, 0.0);
		glVertex3f(random + (- range / 2), random + range / 2, 0.0);
		glVertex3f(random + (- range), random, 0.0);
		glVertex3f(random + (- range / 2), random + (- range / 2), 0.0);
	}
	else if (random >= 0) {
		glVertex3f(random, random, 0.0);
		glVertex3f(random + (range / 2), random + (range / 2), 0.0);
		glVertex3f(random + range, random, 0.0);
		glVertex3f(random + (range / 2), random - (range / 2), 0.0);
	}
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Diamond Green 2
	glNewList(object + 4, GL_COMPILE);
	range = 20;
	random = rand() % 50 + (-50);

	glColor3f(0.0, 0.8, 0.4);
	glBegin(GL_POLYGON);
	if (random < 0) {
		glVertex3f(random, random, 0.0);
		glVertex3f(random + (-range / 2), random + range / 2, 0.0);
		glVertex3f(random + (-range), random, 0.0);
		glVertex3f(random + (-range / 2), random + (-range / 2), 0.0);
	}
	else if (random >= 0) {
		glVertex3f(random, random, 0.0);
		glVertex3f(random + (range / 2), random + (range / 2), 0.0);
		glVertex3f(random + range, random, 0.0);
		glVertex3f(random + (range / 2), random - (range / 2), 0.0);
	}
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Triangle Orange 1
	glNewList(object + 5, GL_COMPILE);
	range = 5;
	random = rand() % 50 + (-50);

	glColor3f(1.0, 0.5, 0.2);
	glBegin(GL_TRIANGLES);
	glVertex3f(random, random, 0.0);
	glVertex3f(random + (range / 2), random + range, 0.0);
	glVertex3f(random + range, random, 0.0);
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	// Triangle Orange 2
	glNewList(object + 6, GL_COMPILE);
	range = 15;
	random = rand() % 50 + (-50);

	glColor3f(1.0, 0.5, 0.2);
	glBegin(GL_TRIANGLES);
	glVertex3f(random, random, 0.0);
	glVertex3f(random + (range / 2), random + range, 0.0);
	glVertex3f(random + range, random, 0.0);
	glEnd();
	glTranslatef(5.0, 0.0, 0.0);
	glEndList();

	glClearColor(1.0, 1.0, 1.0, 0.0);
}

// Drawing routine.
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);

	glPushMatrix();
	glTranslatef(0.0, 0.0, -70.0);
	glCallLists(9, GL_INT, offsets);
	glPopMatrix();

	glFlush();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

// Main routine.
int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(700, 700);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181002-Labwork3.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}
