#pragma once
#include "object.h"
class rectangle :public object{
	private:
		//tinggi (h) dan lebar objek(w)
		float w;
		float h;

	public:
		//constructor persegi
		rectangle();

		//override dari fungsi draw
		void draw();

		//setter getter tinggi dan lebar objek
		void setW(float width);
		void setH(float height);
		float getW();
		float getH();

		//translate
		void translate(float tsx, float tsy);
};

