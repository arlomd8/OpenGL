/////////////////////////////////////////////////////////////////////////////////////         
// 4210181018-Labwork3.cpp
//
// Program ini digunakan untuk mendemonstrasikan eksekusi perintah penggambaran banyak objek yang sama menggunakan display list
// 
//
// ARLO MARIO DENDI
/////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <stdio.h>

#include <GL/glew.h>
#include <GL/freeglut.h> 


// VARIABEL GLOBAL


static unsigned int base; // DEKLARASI INDEX BASE UNTUK DISPLAY LIST
static unsigned int offsets[9] = { 0, 2, 1, 1, 0, 2, 3, 4, 5 }; // ARRAY UNTUK MENYIMPAN OFFSET DISPLAY LIST\

float random()
{
	
	float random;
	return random = rand() % 10 + 10;
}

int randomScale()
{
	
	int random;
	return random = rand() % 5 + 5;
}

float randomRotation()
{
	float random;
	return random = rand() % (45 + 1) + 0;
}

//INISIALISASI
static void setup(void)
{
	

	// MENGEMBALIKAN NILAI PERTAMA DARI TIGA INDEKS YANG BERURUTAN SEBAGAI NILAI DASAR
	base = glGenLists(9);

	// MEMBUAT LIST
	glNewList(base, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 1

	// CETAKAN SEGITIGA ORANGE
	glColor3f(0.929, 0.49, 0.192);
	glBegin(GL_TRIANGLES);
	glVertex2f(10.0, 10.0);
	glVertex2f(20.0, 10.0);
	glVertex2f(15.0, 20.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glEndList();

	glNewList(base + 1, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 2

	// CETAKAN PERSEGI HIJAU
	glColor3f(0.573, 0.816, 0.314);
	glBegin(GL_POLYGON);
	glVertex2f(15.0, 10.0);
	glVertex2f(20.0, 15.0);
	glVertex2f(15.0, 20.0);
	glVertex2f(10.0, 15.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glEndList();

	
	glNewList(base + 2, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 3

	// CETAKAN PERSEGI BIRU
	glColor3f(0.267, 0.447, 0.769);
	glBegin(GL_POLYGON);
	glVertex2f(10.0, 10.0);
	glVertex2f(20.0, 10.0);
	glVertex2f(20.0, 20.0);
	glVertex2f(10.0, 20.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glEndList();


	glNewList(base + 3, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 4

	// CETAKAN PERSEGI MERAH 
	glColor3f(1, 0, 0);
	glBegin(GL_POLYGON);
	glVertex2f(10.0, 10.0);
	glVertex2f(20.0, 10.0);
	glVertex2f(20.0, 20.0);
	glVertex2f(10.0, 20.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glEndList();

	glNewList(base + 4, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 5

	// CETAKAN SEGITIGA HITAM
	glColor3f(0, 0, 0);
	glBegin(GL_TRIANGLES);
	glVertex2f(10.0, 10.0);
	glVertex2f(20.0, 10.0);
	glVertex2f(15.0, 20.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glEndList();

	glNewList(base + 5, GL_COMPILE); // MENGISI LIST DENGAN OBJEK CETAKAN 6

	// CETAKAN BELAH KETUPAT ABU - ABU
	glColor3f(0.5, 0.5, 0.5);
	glBegin(GL_POLYGON);
	glVertex2f(15.0, 10.0);
	glVertex2f(20.0, 15.0);
	glVertex2f(15.0, 20.0);
	glVertex2f(10.0, 15.0);
	glEnd();
	glTranslatef(15.0, 0.0, 0.0);
	glRotatef(45, 0, 0, 1);
	glEndList();


	glClearColor(1.0, 1.0, 1.0, 0.0);
}

// PENGGAMBARAN
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT); 
	glColor3f(1.0, 1.0, 1.0);
	srand((unsigned)time(0));
	float skala	 = randomScale();
	std::cout << random();

	// glPushMatrix() dan glPopMatrix() DIGUNAKAN UNTUK PUSH DAN POP STACK MATRIX YANG ADA
	// glScalef() DIGUNAKAN UNTUK MEN-SKALAKAN OBJEK
	// glTranslatef() DIGUNAKAN UNTUK MENTRANSLASIKAN OBJEK
	// glCallList() DIGUNAKAN UNTUK MEMANGGIL PERINTAH UNTUK MENGGAMBAR OBJEK YANG ADA DI DISPLAY LIST


	//PERSEGI BIRU
	glPushMatrix(); 
	glTranslatef(0.0, 100.0, 0.0); 
	glScalef(5.0, 5.0, 0.0); 
	glCallList(base + 2); 
	glPopMatrix();

	//PERSEGI BIRU
	glPushMatrix();
	glTranslatef(80.0, 100.0, 0.0);
	glScalef(2.0, 2.0, 0.0);
	glCallList(base + 2); 
	glPopMatrix();

	//PERSEGI HIJAU
	glPushMatrix();
	glTranslatef(85.0, 70.0, 0.0);
	glScalef(2.0, 2.0, 0.0);
	glCallList(base + 1); 
	glPopMatrix();

	//PERSEGI BIRU
	glPushMatrix();
	glTranslatef(90.0, 15.0, 0.0);
	glScalef(5.0, 5.0, 0.0);
	glCallList(base + 2);
	glPopMatrix();

	//SEGITIGA ORANGE
	glPushMatrix();
	glTranslatef(85.0, 80.0, 0.0);
	glScalef(5.0, 5.0, 0.0);
	glCallList(base); 
	glPopMatrix();
	
	//PERSEGI BIRU
	glPushMatrix();
	glTranslatef(140.0, 180.0, 0.0);
	glScalef(1.0, 1.0, 0.0);
	glCallList(base + 2); 
	glPopMatrix();    

	//SEGITIGA ORANGE
	glPushMatrix();
	glTranslatef(155.0, 170.0, 0.0);
	glScalef(2.0, 2.0, 0.0);
	glCallList(base); 
	glPopMatrix();

	//PERSEGI HIJAU
	glPushMatrix();
	glTranslatef(160.0, 62.5, 0.0);
	glScalef(4.0, 4.0, 0.0);
	glCallList(base + 1); 
	glPopMatrix();

	//PERSEGI BIRU
	glPushMatrix();
	glTranslatef(170.0, 100.0, 0.0);
	glScalef(5.0, 5.0, 0.0);
	glCallList(base + 2); 
	glPopMatrix();

	//PERSEGI MERAH (RANDOM)
	glPushMatrix();
	glTranslatef( random(), random(), 0.0);
	glScalef(skala, skala , 0.0);
	glRotatef(randomRotation() ,0 , 0 , 1 );
	glCallList(base + 3);
	glPopMatrix();

	//SEGITIGA HITAM (SANGAT RANDOM)
	glPushMatrix();
	glTranslatef(random(), random(), 0.0);
	glScalef(randomScale(), randomScale(), 0.0);
	glRotatef(randomRotation(), random(), random(), random());
	glCallList(base + 4);
	glPopMatrix();

	//PERSEGI BIRU (RANDOM)
	glPushMatrix();
	glTranslatef(random(), random(), 0.0);
	glScalef(skala, skala, 0.0);
	glRotatef(randomRotation(), 0, 0, 1);
	glCallList(base + 2);
	glPopMatrix();

	//PERSEGI HITAM (RANDOM)
	glPushMatrix();
	glTranslatef(random(), random(), 0.0);
	glScalef(skala, skala, 0.0);
	glRotatef(randomRotation(), 0, 0, 1);
	glCallList(base + 4);
	glPopMatrix();

	//PERSEGI MERAH (RANDOM)
	glPushMatrix();
	glTranslatef(random(), random(), 0.0);
	glScalef(skala, skala, 0.0);
	glRotatef(randomRotation(), 0, 0, 1);
	glCallList(base + 3);
	glPopMatrix();

	//BELAH KETUPAT ABU ABU (RANDOM)
	glPushMatrix();
	glTranslatef(random(), random(), 0.0);
	glScalef(skala, skala, 0.0);
	glRotatef(randomRotation(), 0, 0, 1);
	glCallList(base + 5);
	glPopMatrix();

	glLoadIdentity(); //MEMUAT MATRIX IDENTITAS


	// MENENTUKAN BASE YANG AKAN DIGUNAKAN DALAM PEMANGGILAN DISPLAY LIST YANG BERKELANJUTAN
	glListBase(base);


	// MENGEKSEKUSI 6 DISPLAY LIST YANG MEMILIKI OFFSET DARI BASE YANG BERADA DALAM ARRAY OFFSET
	glCallLists(9, GL_INT, offsets); 

	glFlush(); //PERINTAH UNTUK RENDERING
}



// SUDUT PANDANG
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 300.0, 0.0, 300.0, -1.0, 1.0); //UKURAN ORTHOGONAL DIGANTI MENJADI RIGHT 300 , TOP 300
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// MENERIMA INPUT KEYBOARD
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

// MAIN FUNC
int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(800, 800); // UKURAN WINDOW DIGANTI MENJADI 800 X 800 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Labwork 3.cpp"); // NAMA WINDOW DIGANTI MENJADI 4210181018_LABWORK 3.CPP
	glutDisplayFunc(drawScene); //MEMANGGIL drawScene()
	glutReshapeFunc(resize); //MEMANGGIL resize()
	glutKeyboardFunc(keyInput); //MEMANGGIL keyInput()

	glewExperimental = GL_TRUE;
	glewInit(); 

	setup(); //MEMANGGIL SETUP

	glutMainLoop();
}
