// ANALISIS MEMBUAT FUNGSI gluLookAt()
// 4210181018 - ARLO MARIO DENDI
// MATA KULIAH : PENGANTAR GRAFIKA KOMPUTER
// DOSEN PENGAMPU : HALIMATUS SA'DYAH

// 4210181018_Camera.cpp

// SUMBER : https://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/
// SUMBER : https://www.youtube.com/watch?v=RKJfg7Q7EzQ


#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h> 

// Koordinat posisi Camera
float camX = 0, camY = 0, camZ = 0; //Semakin kecil camZ maka semakin kecil perputaran pada poros

// Koordinat posisi Objek Target yang dilihat (Line of Sight) / Reference point
float lineX = 0, lineY = 0, lineZ = -1; // LineY = 0 karena berputar terhadap sumbu Y

// Arah Vector Up (Posisi Kepala)
float upX = 0 , upY = 1 , upZ = 0; //jika upY = -1 maka objek akan tampak terbalik karena posisi camera berada di bawah

// Sudut rotasi
float angle; // semakin besar angle maka semakin besar sudut yang diambil untuk berputar

// FUNGSI MENGGAMBAR.
void drawScene(void) {
	glClear(GL_COLOR_BUFFER_BIT); //MEMBERSIHKAN MENGGUNAKAN BUFFER
	glColor3f(0.0, 0.0, 0.0); // MEMBERI WARNA GARIS ATAUPUN FILL PADA OBJECT (TINTA)
	glLoadIdentity(); 

	//MODELLING TRANSFORM
	glTranslatef(0.0, 0.0, -15.0); // TRANSLASI SUMBU Z


	//PEMANGGILAN FUNGSI GLU LOOK AT 
	gluLookAt(camX, camY, camZ,
			  camX + lineX, camY, camZ + lineZ,
			  upX, upY, upZ); 

	//VIEWING TRANSFORM
	glutWireTeapot(5.0); // PENGGAMBARAN MODEL OBJEK POCI

	glFlush(); // EKSEKUSI PERINTAH UNTUK RENDERING 
}

void setup(void) {
	glClearColor(1.0, 1.0, 1.0, 0.0); // MEMBERSIHKAN KANVAS DENGAN WARNA PUTIH
}

void resize(int w, int h) {
	glViewport(0, 0, w, h); // MENDEFINISIKAN VIEWPORT 
	glMatrixMode(GL_PROJECTION); // MENDEFINISIKAN PROPERTI DARI CAMERA YANG MELIHAT OBJEK PADA WORLD COORDINATE
	glLoadIdentity();
	glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 1000.0); //MENGGAMBARKAN MATRIX PERSPEKTIF YANG DAPAT MENGHASILKAN PROYEKSI PERSPEKTIF

	glMatrixMode(GL_MODELVIEW); // MENDEFINISIKAN BAGAIMANA OBJEK MU DI TRANSFORMASIKAN
}

void keyInput(unsigned char key, int x, int y) {
	float moveFactor = 0.5; // Faktor pengali yang digunakan untuk memajukan dan memundurkan camera sejauh lineX

	switch (key) {
		// CHARACTER YANG DIGUNAKAN DAPAT MENGGUNAKAN UPPERCASE ATAUPUN LOWERCASE

	case 'w':
		camX += lineX * moveFactor;
		camZ += lineZ * moveFactor;
		glutPostRedisplay();
		break;

	case 's':
		camX -= lineX * moveFactor;
		camZ -= lineZ * moveFactor;
		glutPostRedisplay();
		break;

	case 'W':
		camX += lineX * moveFactor;
		camZ += lineZ * moveFactor;
		glutPostRedisplay();
		break;

	case 'S':
		camX -= lineX * moveFactor;
		camZ -= lineZ * moveFactor;
		glutPostRedisplay();
		break;

	case 'a'://Camera Clockwise
		angle += 0.1;
		lineX = sin(angle);
		lineZ = -cos(angle);
		glutPostRedisplay();
		break;

	case 'd'://Camera CounterClockwise
		angle -= 0.1;
		lineX = sin(angle);
		lineZ = -cos(angle);
		glutPostRedisplay();
		break;

	case 'A'://Camera Clockwise
		angle += 0.1;
		lineX = sin(angle);
		lineZ = -cos(angle);
		glutPostRedisplay();
		break;

	case 'D': //Camera CounterClockwise
		angle -= 0.1; 
		lineX = sin(angle);
		lineZ = -cos(angle);
		glutPostRedisplay();
		break;

	default:
		break;

	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Camera.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);


	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}

