
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <math.h>


void drawScene(void)
{
	//LINGKARAN PERTAMA MENGGUNAKAN GL_LINE_LOOP (OUTLINE)
	float r = 20, cx = 25, cy = 50; //INISIALISASI POSISI LINGKARAN 1 
	int num_segments = 180; //JUMLAH SEGMEN ATAU TITIK YANG AKAN DIBUAT

	glClear(GL_COLOR_BUFFER_BIT); 
	glColor3f(0.0, 0.0, 1.0); 
	glBegin(GL_LINE_LOOP); //MENGGUNAKAN PRIMITIF OBJEK GL_LINE_LOOP
	for (int ii = 0; ii < num_segments; ii++) //PERULANGAN YANG DIGUNAKAN UNTUK MENGGAMBAR LINGKARAN
	{
		float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments); //MENGHITUNG SUDUT UNTUK SETIAP TITIK/SEGMENT

		float x = r * cosf(theta); //MENGHITUNG TITIK ATAU VERTEX DI KOORDINAT X
		float y = r * sinf(theta); //MENGHITUNG TITIK ATAU VERTEX DI KOORDINAT Y

		glVertex2f(x + cx, y + cy); //MENG-ASSIGN NILAI VERTEX SESUAI DENGAN PERHITUNGAN KOMPONEN X DAN Y DIATAS

	}
	glEnd();//MENGAKHIRI PERINTAH PENGGAMBARAN

	
	//LINGKARAN KE DUA MENGGUNAKAN TRIANGLE_FAN KARENA BISA DIBERI FILL COLOR
	float r2 = 20, cx2 = 75, cy2 = 50; //INISIALISASI POSISI LINGKARAN 2
	int num_segments2 = 180; //JUMLAH SEGMEN ATAU TITIK YANG AKAN DIBUAT

	glColor3f(0.0, 1.0, 0.0); //KOTAK KE 1 BERWARNA HIJAU
	glBegin(GL_TRIANGLE_FAN); //MENGGUNAKAN PRIMITIF OBJEK GL_TRIANGLE_FAN
	for (int ii2 = 0; ii2 < num_segments2; ii2++) //PERULANGAN YANG DIGUNAKAN UNTUK MENGGAMBAR LINGKARAN
	{
		float theta = 2.0f * 3.1415926f * float(ii2) / float(num_segments2); //MENGHITUNG SUDUT UNTUK SETIAP TITIK/SEGMENT

		float x2 = r2 * cosf(theta); //MENGHITUNG TITIK ATAU VERTEX DI KOORDINAT X
		float y2 = r2 * sinf(theta); //MENGHITUNG TITIK ATAU VERTEX DI KOORDINAT Y

		glVertex2f(x2 + cx2, y2 + cy2);//MENG-ASSIGN NILAI VERTEX SESUAI DENGAN PERHITUNGAN KOMPONEN X DAN Y DIATAS

	}
	glEnd();//MENGAKHIRI PERINTAH PENGGAMBARAN

	glFlush(); //UNTUK MELAKSANAKAN RENDERING
}

void setup(void)
{
	glClearColor(1.0, 0.0, 0.0, 0.0); //UNTUK MEMBERSIHKAN KANVAS DAN MENGGANTI WARNA PADA KANVAS
}

void resize(int w, int h) 
{
	glViewport(0, 0, w, h); //JARAK PANDANG TERHADAP OBJEK

	glMatrixMode(GL_PROJECTION); // MENGGUNAKAN PROYEKSI
	glLoadIdentity(); //MEMUAT MATRIX IDENTITAS
	glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0); // PROYEKSI ORTHOGONAL

	glMatrixMode(GL_MODELVIEW); // MENGGUNAKAN GL_MODELVIEW
	glLoadIdentity(); //MEMUAT MATRIX IDENTITAS
}

void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0); 
		break;
	default:
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv); //INISIALISASI GLUT

	glutInitContextVersion(4, 3); //INISIALISASI DAN SETTING GLUT
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE); //MENGANTISIPASI FUNGSI OPENGL YANG TERDEPRECATED DI VERSI BARU

	//INISIALISASI WINDOW DENGAN UKURAN DAN JUDUL WINDOW
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_PO3");

	
	glutDisplayFunc(drawScene); //MEMANGGIL FUNGSI DISPLAY
	glutReshapeFunc(resize); // MEMANGGIL FUNGSI RESIZE
	glutKeyboardFunc(keyInput); // MEMANGGIL FUNGSI KEYBOARD HANDLER

	//INISIALISASI GLEW
	glewExperimental = GL_TRUE; 
	glewInit();

	setup();//PEMANGGILAN FUNGSI SETUP

	glutMainLoop();// PEMANGGILAN GLUT MAIN LOOP
	
}
