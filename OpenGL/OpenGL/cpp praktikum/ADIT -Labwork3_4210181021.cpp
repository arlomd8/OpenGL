/////////////////////////////////////////////////////////////////////////////////////
// labwork3_4210181021.cpp
//
// Aditya Nur Juang Rahmanda
// Labwork3 - 4210181021
/////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h> //library untuk fungsi srand(), rand(), dll
#include <ctime>

#include <GL/glew.h>
#include <GL/freeglut.h>

// Variabel global
static unsigned int base;

//Membuat fungsi baru untuk random dari range tertentu
float RandomFloat(float, float);
int RandomInt(int, int);

//Fungsi inisialisasi
static void setup(void)
{
	//Menentukan jumlah daftar list yang akan dibuat
	//Nilai dikembalikan pada variabel base
	base = glGenLists(3);

	//Membuat display list pertama
	glNewList(base, GL_COMPILE);
	//Membuat objek segitiga oranye
	glColor3f(1.0, 128.0 / 255.0, 0.0);
	glBegin(GL_TRIANGLES);
	glVertex2f(5.0, 10.0);
	glVertex2f(0.0, 0.0);
	glVertex2f(10.0, 0.0);
	glEnd();
	glEndList();

	//Membuat display list kedua
	glNewList(base + 1, GL_COMPILE);
	//Membuat objek persegi biru
	glColor3f(0.0, 0.25, 0.5);
	glBegin(GL_POLYGON);
	glVertex2f(0.0, 10.0);
	glVertex2f(0.0, 0.0);
	glVertex2f(10.0, 0.0);
	glVertex2f(10.0, 10.0);
	glEnd();
	glEndList();

	//Membuat display list ketiga
	glNewList(base + 2, GL_COMPILE);
	//Membuat objek belah ketupat hijau
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
	glVertex2f(0.0, 5.0);
	glVertex2f(5.0, 0.0);
	glVertex2f(10.0, 5.0);
	glVertex2f(5.0, 10.0);
	glEnd();
	glColor3f(0.0, 0.0, 1.0);

	//Mengakhiri pembuatan list
	glEndList();

	//Menentukan warna background
	glClearColor(1.0, 1.0, 1.0, 0.0);
}

//Fungsi menggambar objek
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();

	//Menentukan variabel base sebagai variabel dasar untuk menampilkan list
	glListBase(base);

	//Variabel untuk menyimpan angka-angka random
	float randomPointX, randomPointY, randomScale;
	int randomOffset;

	//Menggambar 9 objek dengan menggunakan perulangan
	for(int i = 0; i < 9; i++) {
			//Menyimpan pernyataan saat ini pada matrix stack
			glPushMatrix();

			//Mengacak angka dan menyimpan nilainya
			//pada variabel randomPointX dan randomPointY
			randomPointX = RandomFloat(0.0, 80.0);
			randomPointY = RandomFloat(0.0, 80.0);
			//Mengubah posisi x dan y dari objek yang akan dipanggil
			//berdasar nilai dari variabel randomPointX dan randomPointY
			glTranslatef(randomPointX, randomPointY, 0.0);

			//Mengacak angka dan menyimpan nilainya
			//pada variabel randomScale
			randomScale = RandomFloat(0.5, 2.5);
			//Mengubah ukuran dari objek yang akan dipanggil
			//berdasar nilai dari variabel randomScale
			glScalef(randomScale, randomScale, 0.0);

			//Mengacak angka dan menyimpan nilainya
			//pada variabel randomOffset
			randomOffset = RandomInt(0, 2);
			//Memanggil objek yang akan digambar dari List secara acak
			//berdasar nilai dari randomOffset
			glCallList(base + randomOffset);

			//Membuang pernyataan saat ini dari matrix stack
			glPopMatrix();
  }

	glFlush();
}

void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

int RandomInt(int a, int b) {
	return rand() % (b + 1) + a;
}

float RandomFloat(float a, float b) {
	float diff = b - a;
	float random = (float(rand())/float((RAND_MAX)));
	float r = random * diff;
	return a + r;
}

int main(int argc, char **argv)
{
	srand(time(NULL));
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Labwork3_4210181021.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}
