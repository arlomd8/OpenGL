#include "rectangle.h"
#include <GL/glew.h>
#include <GL/freeglut.h> 
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <cmath>
#pragma once

rectangle::rectangle() {
	x = 50;
	y = 50;

	w = 10;
	h = 10;

	sx = 0.5;
	sy = 0.5;

	red = 0;
	green = 0;
	blue = 1;
}

void rectangle::draw(){
	//vertex 2
	float x2 = (w * sx) * cos(angle);
	float y2 = (w * sx) * sin(angle);
	
	//vertex 3
	float x3 = ((w * sx) * cos(angle)) + ((sy * h) * (-sin(angle)));
	float y3 = ((w * sx) * sin(angle)) + ((sy*h)*cos(angle));

	//vertex 4
	float x4 = (sy * h) * (-sin(angle));
	float y4 = (sy * h) * cos(angle);
	
	glPushMatrix();
	glTranslatef(tx, ty, 0);
	glBegin(GL_QUADS);
		glColor3f(red, green, blue);
		glVertex3f(x, y, 0);
		glVertex3f(x+x2,y+y2, 0);//(x+ sx.w,y) 
		glVertex3f(x+x3,y+y3, 0);//(x +sx.w, y+ sy.h)
		glVertex3f(x+x4,y+y4, 0);//(x, y + y + sy.h)
	glEnd();
	glPopMatrix();

}

void rectangle::setW(float width){

	w = width;
}

void rectangle::setH(float height) {

	h = height;
}

float rectangle::getW(){
	return w;
}
float rectangle::getH(){
	return h;
}

void rectangle::translate(float tsx, float tsy) {
	float batas_atas = ty + y + (sy*h);
	float batas_bawah = ty + y;
	float batas_kiri = tx + x;
	float batas_kanan = tx + x + (sx*w);

	if ((batas_atas == 100 && tsy > 0) || (batas_bawah == 0 && tsy < 0))
		tsy = 0;
	else
		if ((batas_kiri == 0 && tsx < 0) || (batas_kanan == 100 && tsx > 0))
			tsx = 0;

	ty += tsy;
	tx += tsx;
}
