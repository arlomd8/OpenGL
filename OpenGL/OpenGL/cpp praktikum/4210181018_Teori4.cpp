﻿/*Untuk mengerjakan tugas Teori, cobalah membuat transformasi geometri untuk titik-titik yang ada pada float vertices.
Float vertices ini berisi vertex "cetakan" dari objek yang akan kita buat. Oleh karena itu, isinya tidak boleh berubah. Sehingga
kita perlu membuat satu array of vertex lagi yang isinya sama persis dengan float vertices1. Sebut saja float move_vertices1. Array ini nantinya berfungsi menyimpan hasil transformasi geometri dari vertices1.

Untuk transformasi geometri, kamu perlu membuat fungsi berikut:
#1 translate
#2 rotatex
#3 rotatey
#4 rotatez
#5 scale
 
Kelima fungsi ini void aja. Isinya sesuai dengan pseudocode di slide. Variabel yang dirubah adalah variabel move_vertices1.
Sebelum tugas ini disubmit, kamu perlu menguji fungsimu untuk dipanggil pada drawscene dan tolong berikan screenshotnya.

Jadi yang perlu disubmit nantinya adalah:
1. Modifikasi dari squareAnnulusAndTriangle yang disertai dokumentasi
2. File docx atau pdf yang berisi screenshot hasil menjalankan ke-5 fungsi.

Format submissionnya adalah:
NRP_Teori4.cpp
NRP_Teori4.pdf

4210181018_Teori4.cpp
*/

#include <iostream>
#include <math.h>
#include <cmath>

#include <GL/glew.h>
#include <GL/freeglut.h>

#define PI 3.14159265


static int isWire = 0; 
static int isTranslating = 0; //boolean untuk mengecek apakah objek bertranslasi
int counter = 0;


// DALAM HAL VERTEX, SAYA MENGUBAH BEBERAPA KOORDINAT VERTEX PADA ARRAY 
// DAN JUGA MENGUBAH UKURAN PROYEKSI ORTHOGONAL

// Kumpulan vertex annulus yang kedua
static float vertices1[] =
{
	70.0, 70.0, 0.0,
	50.0, 50.0, 0.0,
	110.0, 70.0, 0.0,
	130.0, 50.0, 0.0,
	110.0, 110.0, 0.0,
	130.0, 130.0, 0.0,
	70.0, 110.0, 0.0,
	50.0, 130.0, 0.0
};

// Kumpulan vertex annulus yang kedua
static float movedVertices1[] =
{
	-20.0, -20.0, 0.0,
	-40.0, -40.0, 0.0, // pojok kiri bawah 3,4
	 20.0, -20.0, 0.0,
	 40.0, -40.0, 0.0, // pojok kanan bawah 9,10
	 20.0, 20.0, 0.0,
	 40.0, 40.0, 0.0, // pojok kanan atas 15, 16
	 -20.0, 20.0, 0.0,
	 -40.0, 40.0, 0.0 // pojok kiri atas 21, 22
};



// Kumpulan vertex warna untuk annulus
static float colors1[] =
{
	0.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0,
	1.0, 1.0, 0.0,
	1.0, 0.0, 1.0,
	0.0, 1.0, 1.0,
	1.0, 0.0, 0.0
};


// array yang berisi vertex dan warna dari segitiga kesatu yang saling interleaved
static float vertices2AndColors2Interleaved[] =
{
	80.0, 80.0, 0.0, 0.0, 1.0, 1.0,
	100.0, 80.0, 0.0, 1.0, 0.0, 0.0,
	100.0, 100.0, 0.0, 0.0, 1.0, 0.0
};

// array yang berisi vertex dan warna dari segitiga kedua yang saling interleaved
static float movedvertices2AndColors2Interleaved[] =
{
	-10.0, -10.0, 0.0, 0.0, 1.0, 1.0,
	10.0, -10.0, 0.0, 1.0, 0.0, 0.0,
	10.0, 10.0, 0.0, 0.0, 1.0, 0.0
};

// index vertex urut dari Triangle Strip
static unsigned int stripIndices[] = { 0, 1, 2, 3, 4, 5, 6, 7, 0, 1 };





// FUNGSI TRANSLASI UNTUK TRANSFORMASI GEOMETRI
void Translate(float x, float y, float z) {

	// FUNGSI INI AKAN MENGUBAH VERTEX YANG ADA DALAM ARRAY
	// SESUAI DENGAN SUMBU MASING-MASING 
	// HINGGA AKHIRNYA TERJADI TRANSLASI ATAU PERGESERAN

	//Annulus
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i] += x;
	}

	for (int j = 1; j < 24; j += 3) {
		movedVertices1[j] += y;
	}

	for (int k = 2; k < 24; k += 3) {
		movedVertices1[k] += z;
	}

	//Segitiga
	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i] += x;
	}

	for (int j = 1; j < 18; j += 6) {
		movedvertices2AndColors2Interleaved[j] += y;
	}

	for (int k = 2; k < 18; k += 6) {
		movedvertices2AndColors2Interleaved[k] += z;
	}
}

// FUNGSI SCALE PADA TRANSFORMASI GEOMETRY
void Scale(float x, float y, float z) {
	
	// FUNGSI INI AKAN MENGUBAH VERTEX YANG ADA DALAM ARRAY
	// OBJEK AKAN MENGALAMI PERBESARAN MAUPUN PERKECILAN
	// SESUAI DENGAN INPUT USER

	//Annulus
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i] *= x;
	}

	for (int j = 1; j < 24; j += 3) {
		movedVertices1[j] *= y;
	}

	for (int k = 2; k < 24; k += 3) {
		movedVertices1[k] *= z;
	}

	//Segitiga
	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i] *= x;
	}

	for (int j = 1; j < 18; j += 6) {
		movedvertices2AndColors2Interleaved[j] *= y;
	}

	for (int k = 2; k < 18; k += 6) {
		movedvertices2AndColors2Interleaved[k] *= z;
	}
}


// KETIGA FUNGSI ROTATE AKAN MENGUBAH ARRAY DALAM VERTEX 
// SEHINGGA OBJEK AKAN BERPUTAR SEJAUH SUDUT YANG TELAH DITENTUKAN

// FUNGSI ROTASI TERHADAP SUMBU X
void RotateX(float angle) {

	// JIKA ROTASI TERHADAP SUMBU X MAKA SUMBU X DIABAIKAN
	// BERIKUT ADALAH RUMUS YANG DIGUNAKAN 
	// x = x
	// y = ycos(a) - zsin(a)
	// z = ysin(a) + zcos(a) 


	// Rotasi Y untuk Annulus dan segitiga
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i + 1] = (movedVertices1[i + 1] * cos(angle * PI / 180.0)) - 
								(movedVertices1[i + 2] * sin(angle * PI / 180.0));
	}

	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i + 1] = (movedvertices2AndColors2Interleaved[i + 1] * cos(angle * PI / 180.0)) - 
													 (movedvertices2AndColors2Interleaved[i + 2] * sin(angle * PI / 180.0));
	}

	// Rotasi Z untuk Annulus dan segitiga
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i+2] = (movedVertices1[i+1] * sin(angle * PI / 180.0)) + 
							  (movedVertices1[i + 2] * cos(angle * PI / 180.0));
	}

	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i + 2] = (movedvertices2AndColors2Interleaved[i + 1] * sin(angle * PI / 180.0)) + 
													 (movedvertices2AndColors2Interleaved[i + 2] * cos(angle * PI / 180.0));
	}

}

// FUNGSI ROTASI TERHADAP SUMBU Y
void RotateY(float angle) {

	// JIKA ROTASI TERHADAP SUMBU X MAKA SUMBU X DIABAIKAN
	// BERIKUT ADALAH RUMUS YANG DIGUNAKAN 
	// x = xcos(a) + zsin(a)
	// y = y
	// z = -xsin(a) +zcos(a) 

	// Rotasi X untuk annulus dan segitiga
	for (int i = 0; i < 24; i += 3) { 
		movedVertices1[i] = (movedVertices1[i] * cos(angle * PI / 180.0)) + 
							(movedVertices1[i + 2] * sin(angle * PI / 180.0));
	}
	
	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i] = (movedvertices2AndColors2Interleaved[i] * cos(angle * PI / 180.0)) + 
												 (movedvertices2AndColors2Interleaved[i + 2] * sin(angle * PI / 180.0));
	}

	// Rotasi Z untuk annulus dan segitiga
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i+2] = ((-1 * movedVertices1[i]) * sin(angle * PI / 180.0)) + 
							  (movedVertices1[i + 2] * cos(angle * PI / 180.0));
	}

	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i + 2] = ((-1 * movedvertices2AndColors2Interleaved[i]) * sin(angle * PI / 180.0)) + 
													 (movedvertices2AndColors2Interleaved[i + 2] * cos(angle * PI / 180.0));
	}
}

//FUNGSI ROTASI TERHADAP SUMBU Z
void RotateZ(float angle) {

	// JIKA ROTASI TERHADAP SUMBU Z MAKA SUMBU Z DIABAIKAN
	// BERIKUT ADALAH RUMUS YANG DIGUNAKAN 
	// x = xcos⁡(a)  - ysin(a);
	// y = xsin⁡(a) + ycos(⁡a); 
	// z = z;

	// Rotasi X pada annulus dan segitiga
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i] =  (movedVertices1[i] * cos(angle * PI / 180.0)) - 
							 (movedVertices1[i + 1] * sin(angle * PI / 180.0));
	}

	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i] = (movedvertices2AndColors2Interleaved[i] * cos(angle * PI / 180.0)) - 
												 (movedvertices2AndColors2Interleaved[i + 1] * sin(angle * PI / 180.0));
	}


	// Rotasi Y untuk annulus dan segitiga
	for (int i = 0; i < 24; i += 3) {
		movedVertices1[i + 1] = (movedVertices1[i] * sin(angle * PI / 180.0)) + 
								(movedVertices1[i + 1] * cos(angle * PI / 180.0));
	}

	for (int i = 0; i < 18; i += 6) {
		movedvertices2AndColors2Interleaved[i + 1] = (movedvertices2AndColors2Interleaved[i] * sin(angle * PI / 180.0)) + 
													 (movedvertices2AndColors2Interleaved[i + 1] * cos(angle * PI / 180.0));
		
	}
}


void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	if (isWire) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	//PEMANGGILAN FUNGSI TRANSFORMASI GEOMETRI YANG TELAH DIBUAT

	//Translate(-60, -60, 0);
	//Scale(0.5,0.5, 1);
	//RotateX (45);
	//RotateY (45);
	//RotateZ (45);


	// Pointer pada data vertex array dan color untuk annulus kedua
	glVertexPointer(3, GL_FLOAT, 0, movedVertices1);
	glColorPointer(3, GL_FLOAT, 0, colors1);

	// Menggambar annulus kedua dengan menggunakan index yang telah dibuat sebelumnya
	glDrawElements(GL_TRIANGLE_STRIP, 10, GL_UNSIGNED_INT, stripIndices);

	// ANNULUS ORIGINAL
	glVertexPointer(3, GL_FLOAT, 0, vertices1);
	glColorPointer(3, GL_FLOAT, 0, colors1);
	glDrawElements(GL_TRIANGLE_STRIP, 10, GL_UNSIGNED_INT, stripIndices);


	// Pointer pada data vertex array dan color untuk segitiga kedua
	glVertexPointer(3, GL_FLOAT, 6 * sizeof(float), &movedvertices2AndColors2Interleaved[0]);
	glColorPointer(3, GL_FLOAT, 6 * sizeof(float), &vertices2AndColors2Interleaved[3]);

	// Menggambar segitiga kedua dengan menggunakan index yang telah dibuat sebelumnya
	glDrawArrays(GL_TRIANGLES, 0, 3);

	// SEGITIGA ORIGINAL
	glVertexPointer(3, GL_FLOAT, 6 * sizeof(float), &vertices2AndColors2Interleaved[0]);
	glColorPointer(3, GL_FLOAT, 6 * sizeof(float), &vertices2AndColors2Interleaved[3]);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	// perintah untuk mengeksekusi rendering
	glFlush();
}

	

//Fungsi setup untuk membersihkan kanvas
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
}

// Fungsi untuk mengatur ukuran viewport
void resize(int w, int h)
{
	//Ukuran objek telah diubah sehingga memiliki rasio 1:1 

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float size = 200;
	float aspect = (float)w / (float)h;
	glOrtho(-200, 200.0, -200.0, 200.0, -1.0, 1.0); //GANTI UKURAN GL ORTHO
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// fungsi yang menerima input dari keyboard
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case ' ':
		if (isWire == 0) isWire = 1;
		else isWire = 0;
		glutPostRedisplay();
		break;

	case 'T': // Menjadikan huruf T dan t sebagai alat untuk toggle boolean isTranslate
		if (isTranslating == 0) isTranslating = true;
		else isTranslating = 0;
		glutPostRedisplay();
		break;

	case 't': // Menjadikan huruf T dan t sebagai alat untuk toggle boolean isTranslate
		if (isTranslating == 0) isTranslating = true;
		else isTranslating = 0;
		glutPostRedisplay();
		break;

	case 27:
		exit(0);
		break;
	default:
		break;
	}
}


// FUNGSI YANG DIGUNAKAN UNTUK MENGAKSES KUNCI/TOMBOL SPESIAL SEPERTI ARROW KEY
// SELANJUTNYA ADALAH PROSES MENGGERAKKAN OBJEK MENGGUNAKAN ARROW
// ARROW AKAN MEMANGGIL FUNGSI TRANSLATE BERDASARKAN SUMBU YANG DITUJU UNTUK MELAKUKAN TRANSLASI

void specialKey(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_UP:
		if (isTranslating == 1) 
			Translate(0.0, 1.0, 0.0);
		break;

	case GLUT_KEY_DOWN:
		if (isTranslating == 1)
			Translate(0.0, -1.0, 0.0);
		break;

	case GLUT_KEY_LEFT:
		if (isTranslating == 1)
			Translate(-1.0, 0.0, 0.0);
		break;

	case GLUT_KEY_RIGHT:
		if (isTranslating == 1)
			Translate(1.0, 0.0, 0.0);
		break;
	}

	// DISINI ADALAH CARA UNTUK MENDETEKSI COLLISION DARI DUA PERSEGI.
	// DALAM HAL INI SAYA MENGGUNAKAN SISI PALING LUAR DARI ANNULUS
	// ALGORITMA INI BEKERJA DENGAN MEMASTIKAN TIDAK ADA JARAK/CELAH/GAP PADA KEEMPAT SISI ANNULUS
	// NAMUN DENGAN CATATAN TIDAK ADA ROTASI

	float rect1x = (movedVertices1[3] + movedVertices1[9]) / 2;
	float rect1y = (movedVertices1[10] + movedVertices1[16]) / 2;
	float rect1w = movedVertices1[9] - movedVertices1[3];
	float rect1h = movedVertices1[16] - movedVertices1[10];

	float rect2x = (vertices1[3] + vertices1[9]) / 2;
	float rect2y = (vertices1[10] + vertices1[16]) / 2;
	float rect2w = vertices1[9] - vertices1[3];
	float rect2h = vertices1[16] - vertices1[10];

	if (rect1x < rect2x + rect2w &&
		rect1x + rect1w > rect2x &&
		rect1y < rect2y + rect2h &&
		rect1y + rect1h > rect2y)
	{
		counter++;
		std::cout << "Collide Boom!!!" << counter << "\n"; //COLLISION TERDETEKSI
	}

	glutPostRedisplay();
}


// fungsi yang digunakan untuk print instruksi
void printInteraction(void)
{
	std::cout << "Interaction:" << std::endl;
	std::cout << "Press the space bar to toggle between wireframe and filled." << std::endl;
	std::cout << "Press T to toggle the translation mode." << std::endl;
	std::cout << "Use arrow to move the annulus and triangle" << std::endl;
}

// Fungsi main yang digunakan untuk menjalankan runtutan program
int main(int argc, char **argv)
{
	printInteraction();
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Teori4.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutSpecialFunc(specialKey); // menambah fungsi special key

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}
