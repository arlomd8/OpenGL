//////////////////////////////////////////////////////////////////////         
// rotatingHelix3.cpp
// 4210181018_Praktikum4.cpp
// ARLO MARIO DENDI
//
// This program, based on helix.cpp, animates a helix by rotating
// it around its axis using a timer function called by glutTimerFunc().
//
// Interaction:
// Press space to toggle between animation on and off.
// Press the up/down arrow keys to speed up/slow down animation.
//
//Sumanta Guha.
////////////////////////////////////////////////////////////////////// 

#include <cstdlib>
#include <cmath>
#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h> 

#define PI 3.14159265 //MENDEFINISIKAN VARIABEL TETAP

// Global Variabel
static int isAnimate = 0; //Boolean untuk mengaktifkan animasi
static int animationPeriod = 50; // Interval waktu setiap frames.
static float angle = 0.0; // Sudut untuk merotasikan helix

// Drawing routine.
void drawScene(void)
{
	float R = 20.0; // Ukuran jari-jari dari helix

	float t; // Sudut untuk membentuk dan menggambar helix

	glClear(GL_COLOR_BUFFER_BIT); //Membersihkan buffer
	glColor3f(0.0, 0.0, 0.0); // Warna yang akan digunakan untuk menggambar objek
	glPushMatrix(); //Push matrix pada stack


	// Transformasi Geometri berupa Translasi dan Rotasi untuk memanipulasi objek helix.
	// Sumbu dari helix akan diselaraskan sepanjang sumbu y dan kemudian mengembalikannya ke lokasi aslinya

	glTranslatef(0.0, 0.0, -60.0); //Transformasi searah sumbu z
	glRotatef(angle, 0.0, 1.0, 0.0); // Merotasikan Helix terhadap sumbu y dengan sudut yang akan berubah ubah setiap pemanggilan fungsi increaseAngle()
	glTranslatef(0.0, 0.0, 60.0); //Transformasi searah sumbu z ( kembali ke posisi awal)

	// Menggambar Objek Helix
	glBegin(GL_LINE_STRIP); // Memulai penggambaran menggunakan primitive GL_LINE_STRIP
	for (t = -10 * PI; t <= 10 * PI; t += PI / 20.0)
		glVertex3f(R * cos(t), t, R * sin(t) - 60.0); //Rumus dalam menggambar helix
	glEnd(); // Mengakhiri eksekusi penggambaran

	glPopMatrix(); //Pop matrix dari stack
	glutSwapBuffers(); //Fungsi yang digunakan untuk menukar buffer
}

// Initialization routine.
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); // Membersihkan Canvas
}

//Mengatur Frustum sebagai sudut pandang
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Fungsi yang dipanggil secara rutin untuk memperbesar sudut rotasi helix
void increaseAngle(void)
{
	angle += 5.0; // Nilai angle akan bertambah 5 setiap pemanggilan
	if (angle > 360.0) angle -= 360.0; // Jika nilai angle melebihi 360 derajat maka akan dikurangi dengan 360 derajat
}

// Fungsi yang digunakan untuk mengatur animasi 
void animate(int value)
{
	if (isAnimate) // Mengecek apakah boolean tersebut aktif
	{
		increaseAngle(); // Pemanggilan fungsi untuk melakukan rotasi objek helix terhadap sumbu y

		glutPostRedisplay(); // Menandai Window saat ini untuk ditampilkan kembali
		glutTimerFunc(animationPeriod, animate, 1); // Mendaftarkan panggilan balik timer yang akan dipicu dalam jumlah millisecond tertentu.
	}
}

// Fungsi yang digunakan untuk menerima input keyboard
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case ' ': // Tekan spasi 
		if (isAnimate) isAnimate = 0; // Untuk menonaktifkan boolean
		else
		{
			isAnimate = 1; // Mengaktifkan Boolean
			animate(1); // Memanggil fungsi animate() dengan parameter 1
		}
		break;
	default:
		break;
	}
}

// Pemanggilan rutin untuk menerima input tombol non-ASCII
void specialKeyInput(int key, int x, int y)
{
	if (key == GLUT_KEY_DOWN) animationPeriod += 5; // Menambah interval waktu setiap frames sehingga perputarannya semakin melambat
	if (key == GLUT_KEY_UP)
		if (animationPeriod > 5) animationPeriod -= 5; // Mengurangi interval waktu setiap frames sehingga perputarannya semakin cepat
	glutPostRedisplay(); // Menandai Window saat ini untuk ditampilkan kembali
}

// Fungsi rutin untuk menampilkan panduan menggunakan animasi
void printInteraction(void)
{
	std::cout << "Interaction:" << std::endl;
	std::cout << "Press space to toggle between animation on and off." << std::endl
		<< "Press the up/down arrow keys to speed up/slow down animation." << std::endl;
}

// Fungsi main yang digunakan untuk menjalankan semua fungsi perintah yang telah dibuat sebelumnya sesuai dengan urutan
int main(int argc, char** argv)
{
	printInteraction(); // Memanggil fungsi printInteraction()
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Praktikum4.cpp");
	glutDisplayFunc(drawScene); // Memanggil fungsi drawScene()
	glutReshapeFunc(resize); // Memanggil fungsi resize()  untuk mengatur ulang ukuran sudut pandang
	glutKeyboardFunc(keyInput); // Memanggil fungsi keyInput()
	glutSpecialFunc(specialKeyInput); // Memanggil fungsi khusus specialKeyInput()

	glewExperimental = GL_TRUE;
	glewInit();

	setup(); // Memanggil fungsi setup()

	glutMainLoop();
}
