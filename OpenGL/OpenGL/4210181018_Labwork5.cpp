///////////////////////////////////////////////////////////////////////////////////////////////////////
// texturedSquare.cpp
//
// This stripped-down program shows how to load both external and program-generated images as textures.
//
// Interaction:
// Press the left and right arrow keys to rotate the square.
// Press space to toggle between textures.
// Press delete to reset.
//
// Sumanta Guha
//
// Texture Credits: See ExperimenterSource/Textures/TEXTURE_CREDITS.txt
//
//
// ARLO MARIO DENDI
// 4210181018
// 4210181018_Labwork5.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <GL/freeglut.h> 

#include "getBMP.h"

// Globals.
static unsigned int texture[7]; // Deklarasi Array untuk texture
static unsigned char chessboard[64][64][4]; // Array penyimpanan Chessboard.
static float angle = 0.0; // Sudut yang digunakan untuk rotasi
static int id = 0; // id yang digunakan untuk mengidentifikasi texture
static int isTexture = 1; //Boolean untuk display textur atau tidak

// Memuat Texture External
void loadTextures()
{
	// Deklarasi Pointer Array Gambar
	imageFile *image[6];

	// Load Gambar bitmap dengan format .bmp
	image[0] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/canLabel.bmp");
	image[1] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/launch.bmp");
	image[2] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/cray2.bmp");
	image[3] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/trees.bmp");
	image[4] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/sky.bmp");
	image[5] = getBMP("D:/OpenGL/OpenGL/OpenGL/Textures/grass.bmp");

	//TEXTURE KE 1
	glBindTexture(GL_TEXTURE_2D, texture[0]); //Bind ID Texture

	//Assign Gambar Menjadi Texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[0]->width, image[0]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[0]->data); 

	//Texture Wrapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //MENGGUNAKAN GL_REPEAT (default)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); //MENGGUNAKAN GL_REPEAT (default)

	//Texture Filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); //MENGGUNAKAN GL_NEAREST (default)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //MENGGUNAKAN GL_NEAREST (default)

	//TEXTURE KE 2
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[1]->width, image[1]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[1]->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); //MENGGUNAKAN GL_MIRRORED_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); //MENGGUNAKAN GL_MIRRORED_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//TEXTURE KE 3
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[2]->width, image[2]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[2]->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //MENGGUNAKAN GL_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //MENGGUNAKAN GL_LINEAR

	//TEXTURE KE 4
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[3]->width, image[3]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[3]->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//TEXTURE KE 5
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[4]->width, image[4]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[4]->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//TEXTURE KE 6
	glBindTexture(GL_TEXTURE_2D, texture[5]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[5]->width, image[5]->height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, image[5]->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

}

// Membuat Texture Internal Chessboard sebesar 64x64
void createChessboard(void)
{
	int i, j;
	for (i = 0; i < 64; i++)
		for (j = 0; j < 64; j++)
			if ((((i / 8) % 2) && ((j / 8) % 2)) || (!((i / 8) % 2) && !((j / 8) % 2)))
			{
				chessboard[i][j][0] = 0x00;
				chessboard[i][j][1] = 0x00;
				chessboard[i][j][2] = 0x00;
				chessboard[i][j][3] = 0xFF;
			}
			else
			{
				chessboard[i][j][0] = 0xDD;
				chessboard[i][j][1] = 0xDD;
				chessboard[i][j][2] = 0xDD;
				chessboard[i][j][3] = 0xFF;
			}
}

// Memuat Chessboard yang telah dibuat sebelumnya
void loadChessboardTexture()
{
	//Memanggil Fungsi untuk membuat Texture Internal
	createChessboard();

	//TEXTURE KE 7
	glBindTexture(GL_TEXTURE_2D, texture[6]); //Bind ID Texture Internal ke dalam Array Texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, chessboard); //Assign Texture Internal
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);// Texture Wrapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //Texture Filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}


//Fungsi yang digunakan untuk inisialisasi
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); //Membersihkan Kanvas

	// Membuat Texture sebanyak 7 Texture
	glGenTextures(7, texture);

	//Pemanggilan fungsi yang memuat Texture Eksternal
	loadTextures();

	//Memanggil fungsi yang memuat Texture Internal
	loadChessboardTexture();

	// Menentukan nilai texture yang kemudian digabungkan dengan warna permukaan objek
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	//Menyalakan mode untuk texturing 2D pada open GL
	glEnable(GL_TEXTURE_2D);
}

// Fungsi untuk menggambar Objek beserta Texture
void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT); // Membersihkan Buffer

	glLoadIdentity();
	gluLookAt(0.0, 0.0, 20.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glRotatef(angle, 0.0, 1.0, 0.0);

	// TEXTURE MAPPING UNTUK SETIAP SISI KUBUS
	// SERTA PEMBUATAN OBJEK KUBUS 

	// VERTEX KUBUS 
	// TITIK A (-10, -10, 0)
	// TITIK B (10, -10, 0)
	// TITIK C (10, 10, 0)
	// TITIK D (-10, 10, 0)
	// TITIK E (-10, -10, -20)
	// TITIK F (10, -10, -20)
	// TITIK G (10, 10, -20)
	// TITIK H (-10, 10, -20)

	if (isTexture) { // KONDISI KETIKA OBJEK BERTEKSTUR (IS TEXTURE = 1)
		//SISI DEPAN
		glBindTexture(GL_TEXTURE_2D, texture[0]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI DEPAN
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, 0.0); // Titik C
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D 
		glEnd();

		//SISI BELAKANG
		glBindTexture(GL_TEXTURE_2D, texture[1]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI BELAKANG
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0); // Titik F
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glEnd();

		//SISI KANAN
		glBindTexture(GL_TEXTURE_2D, texture[2]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI KANAN
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0); // Titik F
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, 10.0, 0.0);  // Titik C 
		glEnd();


		//SISI KIRI
		glBindTexture(GL_TEXTURE_2D, texture[3]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI KIRI
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H 
		glEnd();

		//SISI ATAS
		glBindTexture(GL_TEXTURE_2D, texture[4]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI ATAS
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, 10.0, 0.0); // Titik C
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H 
		glEnd();

		//SISI BAWAH
		glBindTexture(GL_TEXTURE_2D, texture[5]); //BIND TEXTURE SESUAI DENGAN ID PADA SISI BAWAH
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0); // Titik F
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B 
		glEnd();

	}

	else{ //KONDISI KETIKA TIDAK BERTEXTURE (IS TEXTURE = 0)
		glBindTexture(GL_TEXTURE_2D, texture[6]); //BIND TEXTURE SESUAI DENGAN ID (CHESSBOARD) PADA SEMUA SISI
		//SISI DEPAN
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, 0.0); // Titik C
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D 
		glEnd();

		//SISI BELAKANG
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0);// Titik F
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glEnd();

		//SISI KANAN
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0); // Titik F
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, 10.0, 0.0);  // Titik C 
		glEnd();


		//SISI KIRI
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H 
		glEnd();

		//SISI ATAS
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-10.0, 10.0, 0.0); // Titik D
		glTexCoord2f(1.0, 0.0);
		glVertex3f(10.0, 10.0, 0.0); // Titik C
		glTexCoord2f(1.0, 1.0);
		glVertex3f(10.0, 10.0, -20.0); // Titik G
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-10.0, 10.0, -20.0); // Titik H 
		glEnd();

		//SISI BAWAH
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(10.0, -10.0, -20.0); // Titik F
		glTexCoord2f(1.0, 0.0);
		glVertex3f(-10.0, -10.0, -20.0); // Titik E
		glTexCoord2f(1.0, 1.0);
		glVertex3f(-10.0, -10.0, 0.0); // Titik A
		glTexCoord2f(0.0, 1.0);
		glVertex3f(10.0, -10.0, 0.0); // Titik B 
		glEnd();
	}

	glutSwapBuffers();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0); // Tampilan Camera dengan menggunakan frustum pada proyeksi perspektif
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 0, -3, 0, 0, 0, 0, 1, 0);

}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case ' ': // MENGATUR BOOLEAN PADA OBJEK APAKAH BERTEXTURE ATAU TIDAK
		if (isTexture == 0) isTexture = 1;
		else
			isTexture = 0;
		glutPostRedisplay();
		break;
	case 127:
		angle = 0.0; 
		glutPostRedisplay();
		break;
	default:
		break;
	}
}

// Callback routine for non-ASCII key entry.
void specialKeyInput(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT) // UNTUK MEROTASI KE KIRI
	{
		angle -= 5.0;
		if (angle < 0.0) angle += 360.0;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		angle += 5.0; // UNTUK MEROTASI KE KANAN
		if (angle > 360.0) angle -= 360.0;
	}
	glutPostRedisplay();
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
	std::cout << "Interaction:" << std::endl;
	std::cout << "Press the left and right arrow keys to rotate the square." << std::endl
		<< "Press space to toggle between textures." << std::endl
		<< "Press delete to reset." << std::endl;
}

// Main routine.
int main(int argc, char **argv)
{
	printInteraction();
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(700, 700);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("4210181018_Labwork5.cpp");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutSpecialFunc(specialKeyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}